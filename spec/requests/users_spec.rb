RSpec.describe "Users", type: :request do
  let(:user) { create(:user)}
  let(:users) { create_list :user, 5}
  let(:user_attributes) { attributes_for(:user)}

  context 'when valid' do
    let(:headers) { { 'CONTENT_TYPE' => 'application/json' } }
    describe "GET /users" do
      before(:each) { get users_path, headers: headers }

      it { expect(response).to have_http_status(:ok) }
      it { expect(response.content_type).to eq("application/json; charset=utf-8")}
    end

    describe "GET /users/:id" do
      before {get users_path(user)}

      it { expect(response).to have_http_status(:ok) }
    end

    describe 'POST /users' do
      before {post users_path, params: {user: user_attributes.as_json }}

      it { expect(response).to have_http_status(:ok) }
    end

    describe 'PATCH /users' do
      before(:each) do
        put user_path(user.id), params: {user: { email: 'changed-email@gm.com'} }.as_json
        user.reload
      end
      it { expect(response).to have_http_status(:ok) }
      it { expect(user.email).to eq('changed-email@gm.com') }
    end

    describe 'DELETE /user' do
      it "should destroy article" do
        delete user_path(user.id)
        expect(response).to have_http_status(:ok)
      end
    end
  end

  context 'when invalid' do
    let(:headers) {{ "ACCEPT": "application/json" }}

    describe 'GET /user' do
      it 'should not return user with invalid id' do
        assert_raises(ActiveRecord::RecordNotFound) do
          get user_path('invalid_id'), as: :json
        end
      end
    end

    describe 'POST /users' do
      before {post users_path, params: {user: {email: ''} }, headers: headers}

      it 'with invalid params return 422' do
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['status']).to eq('unprocessable_entity')
      end
    end

    describe 'PUT /users' do
      before {put user_path(user.id), params: {user: { email: ''} }, headers: headers}

      it 'with invalid params return 422' do
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['status']).to eq('unprocessable_entity')
      end

      it 'should not find user with invalid id' do
        assert_raises(ActiveRecord::RecordNotFound) do
          put user_path('invalid_id'), headers: headers
        end
      end
    end

    describe 'DELETE /user' do
      it 'should not destroy article' do
        assert_raises(ActiveRecord::RecordNotFound) do
          delete user_path('invalid_id'), as: :json
        end
      end
    end
  end
end
