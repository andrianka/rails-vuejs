RSpec.feature "Delete Users", type: :feature do

  describe 'with js', js: true do

    scenario 'can delete user' do
      user = create(:user)
      user_email = user.email
      user_first_name = user.first_name
      user_last_name = user.last_name

      visit root_path
      expect(page).to have_content(user.email)
      expect(page).to have_button('delete')

      page.accept_confirm('Are you sure you want to delete this user?'){ click_button "delete" }
      msg = page.accept_alert
      expect(msg).to eq ('User was successfully deleted.')
      expect(page).to have_no_content(user_email)
      expect(page).to have_no_content(user_first_name)
      expect(page).to have_no_content(user_last_name)
    end

    scenario 'cancel deleting user' do
      user = create(:user)
      user_email = user.email
      user_first_name = user.first_name
      user_last_name = user.last_name

      visit root_path
      expect(page).to have_content(user.email)
      expect(page).to have_button('delete')
      click_button "delete"
      page.driver.browser.switch_to.alert.dismiss
      expect(page).to have_content(user_email)
      expect(page).to have_content(user_first_name)
      expect(page).to have_content(user_last_name)
    end
  end
end
