RSpec.feature "Creating Users", type: :feature do
  let(:user) { build(:user) }
  describe 'with js', js: true do
    scenario 'can create user' do
      visit "/"
      click_button "New User"
      expect(page).to have_button('Save')
      fill_in 'First Name', with: user.first_name
      fill_in 'Last Name', with: user.last_name
      fill_in 'Email', with: user.email
      fill_in 'Phone', with: user.phone
      fill_in 'Address', with: user.address
      click_on("save_btn")
      # wait_for_ajax
      expect(page).to have_content(user.email)
    end
  end
end
