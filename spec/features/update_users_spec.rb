RSpec.feature "Update Users", type: :feature do

  describe 'with js', js: true do

    scenario 'can update user' do
      user = create(:user)
      old_email = user.email
      old_name = user.first_name
      old_lname = user.last_name
      visit root_path
      expect(page).to have_content(user.email)
      expect(page).to have_button('edit')
      click_button "edit"
      fill_in 'First Name', with: 'Changed name'
      fill_in 'Last Name', with: 'Changed last name'
      fill_in 'Email', with: 'changed_email@ex.com'
      click_on("save_btn")
      expect(page).to have_no_content(old_email)
      expect(page).to have_no_content(old_name)
      expect(page).to have_no_content(old_lname)

      expect(page).to have_content('Changed name')
      expect(page).to have_content('Changed last name')
    end
  end
end
