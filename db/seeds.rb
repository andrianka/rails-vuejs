require 'faker'

15.times do |i|
  User.create(
    email: Faker::Internet.email,
    first_name: Faker::Name.first_name,
    last_name: Faker::Name.last_name,
    phone: Faker::PhoneNumber,
    address: Faker::Address.city
  )
end
